const path = require('path')
const fs   = require('fs')


export class FilePath{
  constructor(){

  }
  map(pathName){
    fs.readdir(path.resolve(pathName),(err,files)=>{
      if(err) return new Error('read file err');
      files.forEach((fileName)=>{
        let file = path.join(pathName,fileName)
        fs.stat(file,(err,stats)=>{
           if(err) return new Error('stat file err')
           if(stats.isFile()){
             console.log('is file')
           }
           if(stats.isDirectory()){
             console.log('is directory')
           }
        })
      })
    })
  }
}