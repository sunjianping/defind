const path = require('path')
module.exports = {  
  entry:{
    index:'./index.js'
  },
  outpath:{
    
    path:path.resolve('./','dist'),
    //publicPath:'',
    filename:'[name]-[hash].js',
  },
  module:{
    rules:[
      {
        test:'/\.js$/',
        use:[{
          loader:'babel-loader',
          exclude: /node_modules/,
          options:{
            modules:true,
          }
        }
          
        ]
      }
    ]
  }
}